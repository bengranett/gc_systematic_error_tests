Summary
=======

* Description: Halpha line flux limited samples with redshift measurement error
* Measured by: Ben
* Mock: Flagship 1.3.2
* Code: LE3

Selection
=========
The first redshift bin has this selection:


    `ra_gal` > 0 AND `ra_gal` < 90 AND `dec_gal` > 0 AND `dec_gal` < 90 AND 
    `observed_redshift_gal` > 0.90 AND `observed_redshift_gal` < 1.10 AND
    `logf_halpha_model3_ext` > -15.699

Notes
=====

* Random catalog size is 10 times sample size constructed by shuffing redshift.
* Survey area is 1 octant of the sky
* The redshift measurement is simulated with the following recipe.
     1. Generate a mock spectrum with the three lines Halpha, NIIa and NIIb with noise.
     2. Convolve the spectrum with a Gaussian and find peaks.  If the peak is above the 1% false detection rate threshold, go on to measure it.
     3. Perform a template fit against the three lines. The line amplitudes in the fit
        are varied with f_Ha/f_tot in the range 0.5-1.0.
* Theory multipoles including a z_error of sigma_z = 0.0016 computed by Ariel (files xx_mean.dat). Assuming ds = 5 Mpc/h
  Format of the files:
   s/(Mpc/h)   xi_0   sigma_0   xi_2   sigma_2   xi_4   sigma_4
* Theoretical (Gaussian) covariance matrices (files xx_covar.dat). Computed using 
  the recipes of Grieb et al. (2016). 


Redshift error cases
--------------------

* `0` - Line detected, no redshift error applied
* `1` - Redshift measured with template fit algorithm
* `2` - Redshfit measurement corrected for NII systematic trend
* `3` - case 1 shuffled
* `4` - case 2 shuffled
* `5` - constant gaussian redshift error applied
    * 0.9-1.1 sigma=0.0016
    * 1.1-1.35 sigma=0.0016
    * 1.35-1.8 sigma=0.0017

Files
=====

File                                | description  | count    | z_min | z_max | z_mean
------------------------------------|--------------|----------|-------|-------|-------
xi_z1_0.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
xi_z1_1.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
xi_z1_2.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
xi_z1_3.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
xi_z1_4.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
xi_z1_5.dat                         | multipole cf | 2059363  | 0.90  | 1.10  | 1.00
------------------------------------|--------------|----------|-------|-------|-------
xi_z2_0.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
xi_z2_1.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
xi_z2_2.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
xi_z2_3.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
xi_z2_4.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
xi_z2_5.dat                         | multipole cf | 2112020  | 1.10  | 1.35  | 1.22
------------------------------------|--------------|----------|-------|-------|-------
xi_z3_0.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
xi_z3_1.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
xi_z3_2.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
xi_z3_3.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
xi_z3_4.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
xi_z3_5.dat                         | multipole cf | 2327672  | 1.35  | 1.80  | 1.55
------------------------------------|----------------|----------|-------|-------|-------
flagshipv2_z3_zerr_ximulti_covar.dat | Gaussian cov  |    --    | 0.90  | 1.10  | 1.00 
flagshipv2_z2_zerr_ximulti_covar.dat | Gaussian cov  |    --    | 1.10  | 1.35  | 1.22
flagshipv2_z1_zerr_ximulti_covar.dat | Gaussian cov  |    --    | 1.35  | 1.80  | 1.55
flagshipv2_z3_zerr_ximulti_mean.dat  | theory xiell  | 3695739  | 0.90  | 1.10  | 1.00  
flagshipv2_z2_zerr_ximulti_mean.dat  | theory xiell  | 3428368  | 1.10  | 1.35  | 1.22  
flagshipv2_z1_zerr_ximulti_mean.dat  | theory xiell  | 3521959  | 1.35  | 1.80  | 1.55  

