Summary
=======

* Description: varying Halpha line flux limit
* Measured by: Ben
* Mock: Flagship 1.3.2
* Code: LE3

Selection
=========
selection:


    SELECT `ra_gal`, `dec_gal`, `observed_redshift_gal`,`logf_halpha_model3_ext` 
    FROM flagship_mock_1_3_2_s 
    WHERE `ra_gal` > 0 AND `ra_gal` < 90 AND `dec_gal` > 0 AND `dec_gal` < 90 
    AND `observed_redshift_gal` > 0.90 AND `observed_redshift_gal` < 1.8 
    AND `logf_halpha_model3_ext` > -15.721


Notes
=====

* Random catalog size is 10 times sample size constructed by shuffing redshift.
* Survey area is 1 octant of the sky


Flux limit cases
--------------------

Limiting Halpha flux limits (log10f, erg/cm2/s)

tag    | log10(f_limit)
-------|----------------
f1     | -15.721
f2     | -15.699
f3     | -15.678

Files
=====

File                        | description  | count    | z_min | z_max | z_mean
----------------------------|--------------|----------|-------|-------|-------
xi_z1_f1.dat                | multipole cf | 4081519  | 0.90  | 1.10  | 1.00
xi_z1_f2.dat                | multipole cf | 3695739  | 0.90  | 1.10  | 1.00
xi_z1_f3.dat                | multipole cf | 3353423  | 0.90  | 1.10  | 1.00
----------------------------|--------------|----------|-------|-------|-------
xi_z2_f1.dat                | multipole cf | 3828952  | 1.10  | 1.35  | 1.22
xi_z2_f2.dat                | multipole cf | 3428368  | 1.10  | 1.35  | 1.22
xi_z2_f3.dat                | multipole cf | 3079079  | 1.10  | 1.35  | 1.22
----------------------------|--------------|----------|-------|-------|-------
xi_z3_f1.dat                | multipole cf | 3982185  | 1.35  | 1.80  | 1.55
xi_z3_f2.dat                | multipole cf | 3521959  | 1.35  | 1.80  | 1.55
xi_z3_f3.dat                | multipole cf | 3127593  | 1.35  | 1.80  | 1.55
