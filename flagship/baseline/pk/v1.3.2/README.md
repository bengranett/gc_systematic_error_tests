PK measurements from Flagship v1.3.2
====================================

Files

* `z_*/`             PK Measurments
* `ps_rsd_*.txt`     linear theory with window function
* `pk_cov_*.txt`     PK covariance based on linear theory

Linear theory is Kaiser without damping

```
Ps(k, mu) = (b + f*mu**2)**2*P(k)
```

# Power spectrum multipoles

`pk_DOAFFT_256_1D.dat`
`pk_<estimator>_<nc>_1D.dat`

- Lbox = 3400 1/h Mpc
- Mass Assigment Scheme: CIC
- Weigting scheme: FKP with Pest = 20000
- Interlacing alias reduction: ON

## estimator

- DOAFFT: distance observer approximation
- YSCFFT: Yamamoto estimator with Scoccimarro FFT method
- YBCFFT: Yamamoto estimator with Bianchi et al FFT method

Don't expect correct quadrupole with DOAFFT.

## nc

Number of density fluctuation grids per dimension
256, 512, 1024

## PK file format

- Column 1: k (bin centre) [h/Mpc]
- Column 2: P0 (monopole)
- Column 3: P2 (quadrupole)
- Column 4: P4 (hexadecapole)
- Column 5: number of independent k modes


# Window function

- pk_DOAFFT_<nc>_window.dat
- monopole power spectrum of the randoms

# PK error format

`pk_cov_hom_<zbin>.txt`

Using signle nbar in each z bin; hom for homogeneous

`pk_cov_<zbin>.txt`
Using redshift dependent nbar(z) # not done yet

Covariance based on linear theory (Kaiser, no damping)

Diagonal in k

- Column 1: k (bin centre) [h/Mpc]
- Column 2: P0 (monopole)
- Column 3: P2 (quadrupole)
- Column 4: P4 (hexadecapole)
- Column 5: Cov(P0, P0)
- Column 6: Cov(P2, P2)
- Column 7: Cov(P4, P4)
- Column 8: Cov(P0, P2)
- Column 9: Cov(P0, P4)
- Column 10:Cov(P2, P4)


# Real-space window multipoles

window/rr_<zbin>_<dilution factor>.txt

RR is diluted by the <dilution factor> to finish RR pair count in a
reasonable time.

Column 1: r_bin_left
Colunn 2: r_bin_right
Column 3: RR0 monopole
Column 4: RR2 quadrupole
Column 5: RR4
Column 6: RR6
Column 7: RR8

RR0 = 1 for infinite homogeneous volume.

# Pk linear convolved with a 3D window function grid

`ps_rsd_<zbin>_<nc>.txt`

```
P(k) = \int d3k' W(k - k') P_linear(k')
```

is computed from the FFT of,

```
xi(k) = W(x) xi_linear(x)
```

on a 3D grid same as the PK estimation

- Column 1: k (bin centre)
- Column 2: P0 (monopole)
- Column 3: P2 (quadrupole)
- Column 4: P4 (hexadecapole)
- Column 5: nmodes; number of independent modes (k grids in upper-half plane)
- Column 6: k_mean (mean in the bin)

# Randoms
Used _fit randoms by Andrea.
Random has x 2 times more points than data.


* z is a simple mean of the z range (z_min + z_max)/2
* b comes from a fitting by Andrea

# Redshift bins

- z_0.9-1.1
	* z: 1.0
	* b: 1.590000e+00
	* f: 8.785951e-01
- z_1.1-1.35
	* z: 1.225
	* b: 1.900000
	* f: 9.076865e-01
- z_1.35-1.80
	* z: 1.575
	* b: 2.22
	* f: 9.376644e-01
