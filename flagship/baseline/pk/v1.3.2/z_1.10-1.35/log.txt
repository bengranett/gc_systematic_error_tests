
# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator DOAFFT
    FFT estimator with Nft = 256
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    On-the-fly mass assignment
    Time read-assign 10.5643
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.038991
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.109292
Power spectrum monopole written: z_1.10-1.35/pk_DOAFFT_256_window.dat
FFT
    0 transforms
    Time FFT_init 0.0068044
    Time FFT 0.823632
Interlacing
    Time interlacing 0.167131
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 0.16957
Power spectrum multipoles written: z_1.10-1.35/pk_DOAFFT_256_1D.dat
Power spectrum 2D written: z_1.10-1.35/pk_DOAFFT_256_2D.dat
Time total 12.1668

# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator DOAFFT
    FFT estimator with Nft = 512
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    On-the-fly mass assignment
    Time read-assign 11.6541
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.307587
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.848878
Power spectrum monopole written: z_1.10-1.35/pk_DOAFFT_512_window.dat
FFT
    0 transforms
    Time FFT_init 0.00795163
    Time FFT 8.72009
Interlacing
    Time interlacing 1.32332
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 1.34073
Power spectrum multipoles written: z_1.10-1.35/pk_DOAFFT_512_1D.dat
Power spectrum 2D written: z_1.10-1.35/pk_DOAFFT_512_2D.dat
Time total 27.1081

# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator YSCFFT
    FFT estimator with Nft = 256
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    On-the-fly mass assignment
    Time read-assign 10.6089
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.0386503
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.10602
Power spectrum monopole written: z_1.10-1.35/pk_YSCFFT_256_window.dat
Compute with Yamamoto-Scoccimarro estimator
    Compute delta2 field
    Allocated a grid with Nft = 256; 135 Mbytes.
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 0.192452
Time Yamamoto Scoccimarro 6.18188
Power spectrum multipoles written: z_1.10-1.35/pk_YSCFFT_256_1D.dat
Time total 17.5806

# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator YSCFFT
    FFT estimator with Nft = 512
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    On-the-fly mass assignment
    Time read-assign 11.7171
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.305557
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.856327
Power spectrum monopole written: z_1.10-1.35/pk_YSCFFT_512_window.dat
Compute with Yamamoto-Scoccimarro estimator
    Compute delta2 field
    Allocated a grid with Nft = 512; 1077 Mbytes.
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 1.53575
Time Yamamoto Scoccimarro 58.6292
Power spectrum multipoles written: z_1.10-1.35/pk_YSCFFT_512_1D.dat
Time total 77.9764

# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator YBCFFT
    FFT estimator with Nft = 256
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    Allocated a grid with Nft = 256; 135 Mbytes.
    On-the-fly mass assignment
    Time read-assign 10.6069
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.0389882
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.106203
Power spectrum monopole written: z_1.10-1.35/pk_YBCFFT_256_window.dat
Computation with Yamamoto-Bianchi estimator
    Compute delta2 field
    Allocated a grid with Nft = 256; 135 Mbytes.
Compute delta4 field
    Allocated a grid with Nft = 256; 135 Mbytes.
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 0.128417
    Time Yamamoto Bianchi 22.9226
Power spectrum multipoles written: z_1.10-1.35/pk_YBCFFT_256_1D.dat
Time total 34.3101

# Starting PowerSpectrum
# ----------------------------
Using 1 thread(s).
Setup power spectrum estimator YBCFFT
    FFT estimator with Nft = 512
    Box size = 3400
    Box center = 1700 1700 1700Parameter shift not found. Using default 0.5

    shift = 0.5
Read catalog
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    Allocated a grid with Nft = 512; 1077 Mbytes.
    On-the-fly mass assignment
    Time read-assign 11.6875
Compute fluctuation
    alpha = 0.499744
    P(k) normalization 8.89264
    alpha = 0.499744
    P(k) normalization 8.89264
    Time fluctuation 0.309268
Compute power spectrum multipoles
    Shot noise subtraction: 0
    Time multipoles 0.850888
Power spectrum monopole written: z_1.10-1.35/pk_YBCFFT_512_window.dat
Computation with Yamamoto-Bianchi estimator
    Compute delta2 field
    Allocated a grid with Nft = 512; 1077 Mbytes.
Compute delta4 field
    Allocated a grid with Nft = 512; 1077 Mbytes.
Compute power spectrum multipoles
    Shot noise subtraction: 1779.88
    Time multipoles 1.0317
    Time Yamamoto Bianchi 213.917
Power spectrum multipoles written: z_1.10-1.35/pk_YBCFFT_512_1D.dat
Time total 233.197
