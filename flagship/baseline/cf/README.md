Summary
=======

* Description: Baseline correlation function of Halpha line flux limited samples.
* Measured by: Andrea
* Mock: Flagship 1.3.2
* Code: LE3

Selection
=========
The first redshift bin has this selection:


    `ra_gal` > 0 AND `ra_gal` < 90 AND `dec_gal` > 0 AND `dec_gal` < 90 AND 
    `observed_redshift_gal` > 0.90 AND `observed_redshift_gal` < 1.10 AND
    `logf_halpha_model3_ext` > -15.699

Notes
=====

* Two random catalogs were used:
    - `*_fit.dat` : Redshift sampled from parametrized redshift distribution model.
    - `*_shuffling.dat` : Redshifts sampled from galaxy catalog.
* Random catalog size is 2 times sample size.
* Survey area is 1 octant of the sky
* Measurements rebinned to ds = 5 Mpc/h by Ariel (files xx_mean.dat). Rebinning 
  performed by weighting each original bin by r^2. Format of the files:
   s/(Mpc/h)   xi_0   sigma_0   xi_2   sigma_2   xi_4   sigma_4
* Theoretical (Gaussian) covariance matrices (files xx_covar.dat). Computed using 
  the recipes of Grieb et al. (2016). 

Files
=====

File                                | description   | count    | z_min | z_max | z_mean
------------------------------------|---------------|----------|-------|-------|-------
mps_0.90-1.10_fit.dat               | multipole cf  | 3695739  | 0.90  | 1.10  | 1.00   
mps_0.90-1.10_shuffling.dat         | multipole cf  | 3695739  | 0.90  | 1.10  | 1.00
mps_1.10-1.35_fit.dat               | multipole cf  | 3428368  | 1.10  | 1.35  | 1.22
mps_1.10-1.35_shuffling.dat         | multipole cf  | 3428368  | 1.10  | 1.35  | 1.22
mps_1.35-1.80_fit.dat               | multipole cf  | 3521959  | 1.35  | 1.80  | 1.55
mps_1.35-1.80_shuffling.dat         | multipole cf  | 3521959  | 1.35  | 1.80  | 1.55
------------------------------------|---------------|----------|-------|-------|-------
flagshipv2_z3_use_ximulti_covar.dat | Gaussian cov  |    --    | 0.90  | 1.10  | 1.00 
flagshipv2_z2_use_ximulti_covar.dat | Gaussian cov  |    --    | 1.10  | 1.35  | 1.22
flagshipv2_z1_use_ximulti_covar.dat | Gaussian cov  |    --    | 1.35  | 1.80  | 1.55
flagshipv2_z3_use_ximulti_mean.dat  | rebinned xiell| 3695739  | 0.90  | 1.10  | 1.00  
flagshipv2_z2_use_ximulti_mean.dat  | rebinned xiell| 3428368  | 1.10  | 1.35  | 1.22  
flagshipv2_z1_use_ximulti_mean.dat  | rebinned xiell| 3521959  | 1.35  | 1.80  | 1.55  
